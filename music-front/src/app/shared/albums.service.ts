import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Album, AlbumData, ApiAlbumData, PublishAlbum } from '../models/album.models';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {
  constructor(private http: HttpClient) {}

  getAlbums() {
    return this.http.get<ApiAlbumData[]>(environment.apiUrl + '/albums').pipe(
      map(response => {
        return response.map(albumData => {
          return new Album(
            albumData._id,
            albumData.title,
            albumData.artist,
            albumData.release,
            albumData.image,
            albumData.isPublished,
          );
        });
      })
    );
  }


  getAlbum(id: string){
    return this.http.get<ApiAlbumData[]>(environment.apiUrl + `/albums?artist=${id}`).pipe(
      map(result =>{
        return result.map(albumData => {
          return new Album(albumData._id, albumData.title, albumData.artist, albumData.release, albumData.image, albumData.isPublished);
        })
      })
    );
  }

  createAlbum(albumData: AlbumData){
    const formData = new FormData();
    Object.keys(albumData).forEach(key =>{
      if(albumData[key] !== null){
        formData.append(key, albumData[key]);
      }
    });
    return this.http.post(environment.apiUrl + '/albums', formData);
  }

  publishAlbum(publishAlbum: PublishAlbum){
    const publish = {
      isPublished: false
    }
    return this.http.post<PublishAlbum>(environment.apiUrl + `/albums/${publishAlbum.id}/publish`, publish);
  }

  removeAlbum(id: string){
    return this.http.delete(environment.apiUrl + `/albums/${id}`);
  }
}
