import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiTrackHistoryData, TrackHistoryData } from '../models/track-history.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TrackHistoryService{
  constructor(private http: HttpClient){}

  getTracksHistory(){
    return this.http.get<ApiTrackHistoryData[]>(environment.apiUrl + '/track-history').pipe(
      map(response =>{
        return response;
      })
    );
  }

  getTrackHistory(id: string){
    return this.http.get<ApiTrackHistoryData[]>(environment.apiUrl + `/track-history?user=${id}`).pipe(
      map(result =>{
        return result;

      })
    );
  }

  addTrack(trackHistoryData: TrackHistoryData){
    console.log(trackHistoryData);
    return this.http.post<ApiTrackHistoryData>(environment.apiUrl + '/track-history', trackHistoryData.track, {
      headers: new HttpHeaders({'Authorization': trackHistoryData.token})
    })
  }
}
