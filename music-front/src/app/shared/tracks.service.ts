import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiTrackData, PublishTrack, Track, TrackData } from '../models/track.model';
import { map } from 'rxjs/operators';
import { PublishAlbum } from '../models/album.models';

@Injectable({
  providedIn: 'root'
})
export class TracksService {
  constructor(private http: HttpClient){}

  getTracks(){
    return this.http.get<ApiTrackData[]>(environment.apiUrl + '/tracks').pipe(
      map(response =>{
        return response.map(trackData => {
          return new Track(
            trackData._id,
            trackData.trackname,
            trackData.album,
            trackData.duration
          );
        });
      })
    );
  }

  getTrack(id: string){
    return this.http.get<ApiTrackData[]>(environment.apiUrl + `/tracks?album=${id}`).pipe(
      map(result =>{
        return result.map(trackData =>{
          return new Track(trackData._id, trackData.trackname, trackData.album, trackData.duration);
        })
      })
    );
  }

  createTrack(trackData: TrackData){
    return this.http.post(environment.apiUrl + '/tracks', trackData);
  }

  publishTrack(publishTrack: PublishTrack){
    const publish = {
      isPublished: true
    }
    return this.http.post<PublishTrack>(environment.apiUrl + `/tracks/${publishTrack.id}/publish`, publish);
  }

  removeTrack(id: string){
    return this.http.delete(environment.apiUrl + `/tracks/${id}`);
  }
}
