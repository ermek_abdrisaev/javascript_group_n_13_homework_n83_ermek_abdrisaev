import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginUserData, RegisterUserData, User } from '../models/user.model';
import { Injectable } from '@angular/core';
import { environment as env} from '../../environments/environment';
import { SocialUser } from 'angularx-social-login';
import { Store } from '@ngrx/store';
import { fbLoginSuccess, loginUserSuccess } from '../store/user.actions';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient, private store: Store) { }

  registerUser(registerUserData: RegisterUserData){
    const formData = new FormData();
    formData.append('email', registerUserData.email);
    formData.append('displayName', registerUserData.displayName);
    formData.append('password', registerUserData.password);

    if (registerUserData.avatar) {
      formData.append('avatar', registerUserData.avatar);
    }
    return this.http.post<User>(env.apiUrl + '/users', formData);
  }

  login(userData: LoginUserData){
    return this.http.post<User>(env.apiUrl + '/users/sessions', userData);
  }

  fbLogin(user: SocialUser){
    return this.http.post<User>(env.apiUrl + '/users/facebookLogin', user);
  }

  logout(){
    return this.http.delete(env.apiUrl + '/users/sessions');
  }

}
