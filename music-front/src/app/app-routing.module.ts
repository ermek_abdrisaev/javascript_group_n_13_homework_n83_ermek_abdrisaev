import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumsComponent } from './pages/albums/albums.component';
import { ArtistsComponent } from './pages/artists/artists.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { TracksComponent } from './pages/tracks/tracks.component';
import { TrackHistoryComponent } from './pages/track-history/track-history.component';
import { NewArtistComponent } from './pages/new-pages/new-artist/new-artist.component';
import { RoleGuardService } from './shared/role-guard.service';
import { NewAlbumComponent } from './pages/new-pages/new-album/new-album.component';
import { NewTrackComponent } from './pages/new-pages/new-track/new-track.component';

const routes: Routes = [
  {path: '', component: ArtistsComponent},
  {path: 'albums', component: AlbumsComponent},
  {path: 'albums/:id', component: AlbumsComponent},
  {
    path: 'new-album',
    component: NewAlbumComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['admin', 'user']}
  },
  {path: 'tracks', component: TracksComponent},
  {
    path: 'new-track',
    component: NewTrackComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['admin', 'user']}
  },
  {path: 'track-history', component: TrackHistoryComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {
    path: 'new-artist',
    component: NewArtistComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['admin', 'user']}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
