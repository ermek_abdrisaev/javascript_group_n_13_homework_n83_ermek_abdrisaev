import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createArtistFailure,
  createArtistRequest,
  createArtistSuccess, deleteArtistFailure, deleteArtistRequest, deleteArtistSuccess,
  fetchArtistsFailure,
  fetchArtistsRequest,
  fetchArtistsSuccess, publishArtistFailure, publishArtistRequest, publishArtistSuccess
} from './artists.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { ArtistsService } from '../shared/artists.service';
import { Router } from '@angular/router';
import { deleteTrackFailure, deleteTrackRequest, deleteTrackSuccess } from './tracks.actions';
import { publishAlbumFailure, publishAlbumRequest, publishAlbumSuccess } from './albums.actions';

@Injectable()
export class ArtistsEffects {
  fetchArtists = createEffect(() => this.actions.pipe(
    ofType(fetchArtistsRequest),
    mergeMap(() => this.artistsService.getArtists().pipe(
      map(artists => fetchArtistsSuccess({artists})),
      catchError(() =>of(fetchArtistsFailure({error: "Something goes wrong!"})))
    ))
  ));

  createArtist = createEffect(() => this.actions.pipe(
    ofType(createArtistRequest),
    mergeMap(({artistData}) => this.artistsService.createArtist(artistData)
      .pipe(
        map(() => createArtistSuccess()),
        tap(() => {
          return this.router.navigate(['/']);
        }),
        catchError(()=>{
          return of(createArtistFailure({
            error: 'Wrong data'
          }));
        })
      )
    )
  ));

  publishArtist = createEffect(() => this.actions.pipe(
    ofType(publishArtistRequest),
    mergeMap(({publish}) => this.artistsService.publishArtist(publish).pipe(
      map( () => publishArtistSuccess()),
      catchError(() => of(publishArtistFailure({error: 'Not published'})))
    ))
  ));

  deleteArtist = createEffect(() => this.actions.pipe(
    ofType(deleteArtistRequest),
    mergeMap(({id}) => this.artistsService.removeArtist(id).pipe(
      map( () => deleteArtistSuccess()),
      catchError(() => of(deleteArtistFailure({error: 'Not deleted'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private artistsService: ArtistsService,
    private router: Router
  ){}
}
