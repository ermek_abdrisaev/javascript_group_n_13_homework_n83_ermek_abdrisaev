import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TrackHistoryService } from '../shared/track-history.service';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HelpersService } from '../shared/helpers.service';
import {
  createTrackHistoryFailure,
  createTrackHistoryRequest,
  createTrackHistorySuccess
} from './track-history.actions';
import { fetchAlbumsFailure } from './albums.actions';

@Injectable()
export class TrackHistoryEffects {

  createTrackHistories = createEffect(() => this.actions.pipe(
    ofType(createTrackHistoryRequest),
    mergeMap(({trackHistory}) => this.trackHistoryService.addTrack(trackHistory).pipe(
      map(trackHistory => createTrackHistorySuccess({trackHistory})),
      tap(() => {
        this.helpers.openSnackBar('Track added to track list');
        void this.router.navigate(['/']);
      }),
      catchError(() => of(createTrackHistoryFailure({error: "Something goes wrong"})))
    ))
  ));

  constructor(
    private actions: Actions,
    private trackHistoryService: TrackHistoryService,
    private router: Router,
    private helpers: HelpersService,
  ) {
  }
}
