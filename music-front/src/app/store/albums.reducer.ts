import { AlbumsState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createAlbumsFailure,
  createAlbumsRequest,
  createAlbumsSuccess,
  deleteAlbumFailure,
  deleteAlbumRequest,
  deleteAlbumSuccess,
  fetchAlbumsFailure,
  fetchAlbumsRequest,
  fetchAlbumsSuccess,
  publishAlbumFailure,
  publishAlbumRequest,
  publishAlbumSuccess
} from './albums.actions';

const initialState: AlbumsState = {
  albums: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  publishLoading: false,
};

export const albumReducer = createReducer(
  initialState,
  on(fetchAlbumsRequest, state => ({...state, fetchLoading: true})),
  on(fetchAlbumsSuccess, (state, {albums}) => ({...state, fetchLoading: false, albums})),
  on(fetchAlbumsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createAlbumsRequest, state => ({...state, createLoading: true})),
  on(createAlbumsSuccess, state => ({...state, createLoading: false})),
  on(createAlbumsFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(deleteAlbumRequest, state => ({...state, createLoading: true})),
  on(deleteAlbumSuccess, state => ({...state, createLoading: false})),
  on(deleteAlbumFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(publishAlbumRequest, state => ({...state, createLoading: true})),
  on(publishAlbumSuccess, state => ({...state, createLoading: false})),
  on(publishAlbumFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

);
