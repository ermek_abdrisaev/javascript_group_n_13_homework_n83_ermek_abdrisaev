import { Album } from '../models/album.models';
import { Artist } from '../models/artist.model';
import { LoginError, RegisterError, User } from '../models/user.model';
import { Track } from '../models/track.model';
import { TrackHistory } from '../models/track-history.model';

export type AlbumsState = {
  albums: Album[];
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  publishLoading: boolean,
};

export type ArtistsState = {
  artists: Artist[],
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  publishLoading: boolean,
};

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError,
};

export type TracksState = {
  tracks: Track[],
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  publishLoading: boolean,
};

export type TrackHistoryState = {
  trackHistories: TrackHistory[],
  fetchLoading: boolean,
  fetchError: null | string,
}


export type AppState ={
  albums: AlbumsState,
  artists: ArtistsState,
  users: UsersState,
  tracks: TracksState,
  trackHistories: TrackHistoryState,
}
