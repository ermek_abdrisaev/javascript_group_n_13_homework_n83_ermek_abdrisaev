import { createReducer, on } from '@ngrx/store';
import {
  createArtistFailure,
  createArtistRequest,
  createArtistSuccess,
  deleteArtistFailure,
  deleteArtistRequest,
  deleteArtistSuccess,
  fetchArtistsFailure,
  fetchArtistsRequest,
  fetchArtistsSuccess, publishArtistFailure, publishArtistRequest, publishArtistSuccess
} from './artists.actions';
import { ArtistsState } from './types';
import { publishAlbumFailure, publishAlbumRequest, publishAlbumSuccess } from './albums.actions';

const initialState: ArtistsState = {
  artists: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  publishLoading: false,
};

export const artistReducer = createReducer(
  initialState,
  on(fetchArtistsRequest, state => ({...state, fetchLoading: true})),
  on(fetchArtistsSuccess, (state, {artists}) =>({...state, fetchLoading: false, artists})),
  on(fetchArtistsFailure, (state, {error}) =>({...state, fetchLoading: false, fetchError: error})),

  on(createArtistRequest, state => ({...state, createLoading: true})),
  on(createArtistSuccess, state => ({...state, createLoading: false})),
  on(createArtistFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(publishArtistRequest, state => ({...state, createLoading: true})),
  on(publishArtistSuccess, state => ({...state, createLoading: false})),
  on(publishArtistFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(deleteArtistRequest, state => ({...state, createLoading: true})),
  on(deleteArtistSuccess, state => ({...state, createLoading: false})),
  on(deleteArtistFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),
)
