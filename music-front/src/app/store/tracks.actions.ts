import { createAction, props } from '@ngrx/store';
import { PublishTrack, Track, TrackData } from '../models/track.model';
import { PublishAlbum } from '../models/album.models';

export const fetchTracksRequest = createAction(
  '[Tracks] Fetch Request',
  props<{id: string}>()
);
export const fetchTracksSuccess = createAction(
  '[Tracks] Fetch Success',
  props<{tracks: Track[]}>()
);
export const fetchTracksFailure = createAction(
  '[Tracks] Fetch Failure',
  props<{error: string}>()
);

export const createTracksRequest = createAction(
  '[Tracks] Create Request',
  props<{trackData: TrackData}>()
);
export const createTracksSuccess = createAction(
  '[Tracks] Create Success'
);
export const createTracksFailure = createAction(
  '[Tracks] Create Failure',
  props<{error: string}>()
);

export const publishTracksRequest = createAction(
  '[Tracks] Publish Request',
  props<{ publish: PublishTrack }>());
export const publishTrackSuccess = createAction(
  '[Tracks] Publish Success');
export const publishTracksFailure = createAction(
  '[Tracks] Delete Failure',
  props<{ error: string }>());

export const deleteTrackRequest = createAction(
  '[Track] Delete Request',
  props<{ id: string }>());
export const deleteTrackSuccess = createAction(
  '[Track] Delete Success');
export const deleteTrackFailure = createAction(
  '[Track] Delete Failure',
  props<{ error: string }>());
