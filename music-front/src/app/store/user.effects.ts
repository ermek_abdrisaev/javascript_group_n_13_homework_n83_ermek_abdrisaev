import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { mergeMap, NEVER, tap, withLatestFrom } from 'rxjs';
import {
  fbLoginFailure,
  fbLoginRequest, fbLoginSuccess,
  loginUserFailure,
  loginUserRequest,
  loginUserSuccess,
  logoutUser,
  logoutUserRequest,
  registerUserRequest,
  registerUserSuccess
} from './user.actions';
import { map } from 'rxjs/operators';
import { UsersService } from '../shared/users.service';
import { HelpersService } from '../shared/helpers.service';
import { AppState } from './types';
import { Store } from '@ngrx/store';

@Injectable()
export class UserEffects {
  constructor(
    private actions: Actions,
    private usersService: UsersService,
    private router: Router,
    private helpers: HelpersService,
    private store: Store<AppState>,
  ) {
  }

  registerUser = createEffect(() => this.actions.pipe(
    ofType(registerUserRequest),
    mergeMap(({userData}) => this.usersService.registerUser(userData).pipe(
      map(user => registerUserSuccess({user})),
      tap(() => {
        this.helpers.openSnackBar('Register successful', 'Got it!');
        void this.router.navigate(['/']);
      }),
    ))
  ));

  loginUser = createEffect(() => this.actions.pipe(
    ofType(loginUserRequest),
    mergeMap(({userData}) => this.usersService.login(userData).pipe(
      map(user => loginUserSuccess({user})),
      tap(() => {
        this.helpers.openSnackBar('Login successful');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(loginUserFailure)
    ))
  ));

  logoutUser = createEffect(() => this.actions.pipe(
    ofType(logoutUserRequest),
    mergeMap(() => {
      return this.usersService.logout().pipe(
        map(() => logoutUser()),
        tap(() => {
          void this.router.navigate(['/']);
          this.helpers.openSnackBar('Logout successful')
        })
      );
    }))
  );

  fbLoginUser = createEffect(() => this.actions.pipe(
    ofType(fbLoginRequest),
    mergeMap(({user}) => this.usersService.fbLogin(user).pipe(
      map(user => fbLoginSuccess({user})),
      tap(() => {
        this.helpers.openSnackBar('Congrats you logged via Facebook!');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(fbLoginFailure)
    ))
  ));
}
