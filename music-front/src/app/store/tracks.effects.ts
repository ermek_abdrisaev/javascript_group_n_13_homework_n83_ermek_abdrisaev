import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TracksService } from '../shared/tracks.service';
import {
  createTracksFailure,
  createTracksRequest,
  createTracksSuccess, deleteTrackFailure, deleteTrackRequest, deleteTrackSuccess,
  fetchTracksFailure,
  fetchTracksRequest,
  fetchTracksSuccess, publishTracksFailure, publishTracksRequest, publishTrackSuccess
} from './tracks.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { publishAlbumFailure, publishAlbumRequest, publishAlbumSuccess } from './albums.actions';

@Injectable()
export class TracksEffects {
  fetchTracks = createEffect(() => this.actions.pipe(
    ofType(fetchTracksRequest),
    mergeMap(id => this.tracksService.getTrack(id.id).pipe(
      map(tracks => fetchTracksSuccess({tracks})),
      catchError(() => of(fetchTracksFailure({error: "Something goes wrong"})))
    ))
  ));

  createTrack = createEffect(() => this.actions.pipe(
    ofType(createTracksRequest),
    mergeMap(({trackData}) => this.tracksService.createTrack(trackData)
      .pipe(
        map(() => createTracksSuccess()),
        tap(() => {
          return this.router.navigate(['/']);
        }),
        catchError(()=>{
          return of(createTracksFailure({
            error: 'Wrong data'
          }));
        })
      )
    )
  ));

  publishTrack = createEffect(() => this.actions.pipe(
    ofType(publishTracksRequest),
    mergeMap(({publish}) => this.tracksService.publishTrack(publish).pipe(
      map( () => publishTrackSuccess()),
      catchError(() => of(publishTracksFailure({error: 'Not published'})))
    ))
  ));

  deleteTrack = createEffect(() => this.actions.pipe(
    ofType(deleteTrackRequest),
    mergeMap(({id}) => this.tracksService.removeTrack(id).pipe(
      map( () => deleteTrackSuccess()),
      catchError(() => of(deleteTrackFailure({error: 'Not deleted'})))
    ))
  ));


  constructor(
    private actions: Actions,
    private tracksService: TracksService,
    private router: Router,
  ){}
}
