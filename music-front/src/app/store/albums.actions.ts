import { createAction, props } from '@ngrx/store';
import { Album, AlbumData, PublishAlbum } from '../models/album.models';

export const fetchAlbumsRequest = createAction(
  '[Albums] Fetch Request',
  props<{id: string}>()
  );
export const fetchAlbumsSuccess = createAction(
  '[Albums] Fetch Success',
  props<{albums: Album[]}>()
);
export const fetchAlbumsFailure = createAction(
  '[Albums] Fetch Failure',
  props<{error: string}>()
);

export const createAlbumsRequest = createAction(
  '[Albums] Create Request',
  props<{albumData: AlbumData}>()
);
export const createAlbumsSuccess = createAction(
  '[Albums] Create Success'
);
export const createAlbumsFailure = createAction(
  '[Albums] Create Failure',
  props<{error: string}>()
);

export const publishAlbumRequest = createAction(
  '[Album] Publish Request',
  props<{ publish: PublishAlbum }>());
export const publishAlbumSuccess = createAction(
  '[Album] Publish Success');
export const publishAlbumFailure = createAction(
  '[Album] Delete Failure',
  props<{ error: string }>());

export const deleteAlbumRequest = createAction(
  '[Albums] Delete Request',
  props<{ id: string }>());
export const deleteAlbumSuccess = createAction(
  '[Albums] Delete Success');
export const deleteAlbumFailure = createAction(
  '[Albums] Delete Failure',
  props<{ error: string }>());
