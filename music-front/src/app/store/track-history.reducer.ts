import { TrackHistoryState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createTrackHistoryFailure,
  createTrackHistoryRequest, createTrackHistorySuccess,
  fetchTrackHistoryFailure,
  fetchTrackHistoryRequest,
  fetchTrackHistorySuccess
} from './track-history.actions';


const initialState: TrackHistoryState = {
  trackHistories: [],
  fetchLoading: false,
  fetchError: null,
};

export const trackHistoryReducer = createReducer(
  initialState,
  on(fetchTrackHistoryRequest, state => ({...state, fetchLoading: true})),
  on(fetchTrackHistorySuccess, (state, {trackHistory}) => ({...state, fetchLoading: false, trackHistory})),
  on(fetchTrackHistoryFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createTrackHistoryRequest, state => ({...state, createLoading: true})),
  on(createTrackHistorySuccess, (state, {trackHistory}) => ({...state, createLoading: false, trackHistory})),
  on(createTrackHistoryFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),
);
