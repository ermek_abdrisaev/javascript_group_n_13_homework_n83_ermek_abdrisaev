import { createAction, props } from '@ngrx/store';
import { ApiTrackHistoryData, TrackHistory, TrackHistoryData } from '../models/track-history.model';

export const fetchTrackHistoryRequest = createAction('[TrackHistories] Fetch Request');
export const fetchTrackHistorySuccess = createAction('[TrackHistories] Fetch Success', props<{trackHistory: TrackHistory[]}>());
export const fetchTrackHistoryFailure = createAction('[TrackHistories] Fetch Failure', props<{error: string}>());

export const createTrackHistoryRequest = createAction('[TrackHistory] Create Request', props<{trackHistory: TrackHistoryData}>());
export const createTrackHistorySuccess = createAction('[TrackHistory] Create Success', props<{trackHistory: ApiTrackHistoryData}>());
export const createTrackHistoryFailure = createAction('[TrackHistory] Create Failure', props<{error: string}>());
