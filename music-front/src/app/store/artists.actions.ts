import { createAction, props } from '@ngrx/store';
import { Artist, ArtistData, PublishArtist } from '../models/artist.model';

export const fetchArtistsRequest = createAction(
  '[Artists] Fetch Request'
);
export const fetchArtistsSuccess = createAction(
  '[Artists] Fetch Success',
  props<{artists: Artist[]}>()
);
export const fetchArtistsFailure = createAction(
  '[Artists] Fetch Failure',
  props<{error: string}>()
);

export const createArtistRequest = createAction(
  '[Artists] Create Request',
  props<{artistData: ArtistData}>()
);
export const createArtistSuccess = createAction(
  '[Artists] Create Success'
);
export const createArtistFailure = createAction(
  '[Artists] Create Failure',
  props<{error: string}>()
);

export const publishArtistRequest = createAction(
  '[Artist] Publish Request',
  props<{ publish: PublishArtist }>());
export const publishArtistSuccess = createAction(
  '[Artist] Publish Success');
export const publishArtistFailure = createAction(
  '[Artist] Delete Failure',
  props<{ error: string }>());

export const deleteArtistRequest = createAction(
  '[Artists] Delete Request',
  props<{ id: string }>());
export const deleteArtistSuccess = createAction(
  '[Artists] Delete Success');
export const deleteArtistFailure = createAction(
  '[Artists] Delete Failure',
  props<{ error: string }>());
