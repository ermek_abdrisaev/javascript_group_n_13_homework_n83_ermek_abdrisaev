import { TracksState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createTracksFailure,
  createTracksRequest,
  createTracksSuccess, deleteTrackFailure, deleteTrackRequest, deleteTrackSuccess,
  fetchTracksFailure,
  fetchTracksRequest,
  fetchTracksSuccess, publishTracksFailure, publishTracksRequest, publishTrackSuccess
} from './tracks.actions';
import { publishAlbumFailure, publishAlbumRequest, publishAlbumSuccess } from './albums.actions';

const initialState: TracksState = {
  tracks: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  publishLoading: false,
};

export const trackReducer = createReducer(
  initialState,
  on(fetchTracksRequest, state => ({...state, fetchLoading: true})),
  on(fetchTracksSuccess, (state, {tracks}) =>({...state, fetchLoading: false, tracks})),
  on(fetchTracksFailure, (state, {error}) =>({...state, fetchLoading: false, fetchError: error})),

  on(createTracksRequest, state => ({...state, createLoading: true})),
  on(createTracksSuccess, state => ({...state, createLoading: false})),
  on(createTracksFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(publishTracksRequest, state => ({...state, createLoading: true})),
  on(publishTrackSuccess, state => ({...state, createLoading: false})),
  on(publishTracksFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(deleteTrackRequest, state => ({...state, createLoading: true})),
  on(deleteTrackSuccess, state => ({...state, createLoading: false})),
  on(deleteTrackFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),
)
