import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  createAlbumsFailure,
  createAlbumsRequest,
  createAlbumsSuccess, deleteAlbumFailure, deleteAlbumRequest, deleteAlbumSuccess,
  fetchAlbumsFailure,
  fetchAlbumsRequest,
  fetchAlbumsSuccess, publishAlbumFailure, publishAlbumRequest, publishAlbumSuccess
} from './albums.actions';
import { AlbumsService } from '../shared/albums.service';
import { Router } from '@angular/router';
import { createArtistFailure, createArtistRequest, createArtistSuccess } from './artists.actions';
import { deleteTrackFailure, deleteTrackRequest, deleteTrackSuccess } from './tracks.actions';

@Injectable()
export class AlbumsEffects {
  fetchAlbums = createEffect(() => this.actions.pipe(
    ofType(fetchAlbumsRequest),
    mergeMap(id => this.albumsService.getAlbum(id.id).pipe(
      map(albums => fetchAlbumsSuccess({albums})),
      catchError(() => of(fetchAlbumsFailure({error: "Something goes wrong"})))
    ))
  ));

  createArtist = createEffect(() => this.actions.pipe(
    ofType(createAlbumsRequest),
    mergeMap(({albumData}) => this.albumsService.createAlbum(albumData)
      .pipe(
        map(() => createAlbumsSuccess()),
        tap(() => {
          return this.router.navigate(['/']);
        }),
        catchError(()=>{
          return of(createAlbumsFailure({
            error: 'Wrong data'
          }));
        })
      )
    )
  ));

  publishAlbum = createEffect(() => this.actions.pipe(
    ofType(publishAlbumRequest),
    mergeMap(({publish}) => this.albumsService.publishAlbum(publish).pipe(
      map( () => publishAlbumSuccess()),
      catchError(() => of(publishAlbumFailure({error: 'Not published'})))
    ))
  ));

  deleteAlbum = createEffect(() => this.actions.pipe(
    ofType(deleteAlbumRequest),
    mergeMap(({id}) => this.albumsService.removeAlbum(id).pipe(
      map( () => deleteAlbumSuccess()),
      catchError(() => of(deleteAlbumFailure({error: 'Not deleted'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private albumsService: AlbumsService,
    private router: Router
  ) {  }
}
