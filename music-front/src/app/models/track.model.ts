export class Track {
  constructor(
    public _id: string,
    public trackname: string,
    public album: string,
    public duration: string,
  ){}
}

export interface TrackData {
  [key: string]: any;
  trackname: string;
  album: string,
  duration: string,
}

export interface ApiTrackData{
  _id: string,
  trackname: string,
  album: string,
  duration: string,
}

export interface PublishTrack {
  id: string,
  isPublished: boolean
}
