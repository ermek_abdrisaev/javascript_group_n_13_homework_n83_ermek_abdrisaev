export class Artist {
  constructor(
    public _id: string,
    public name: string,
    public information: string,
    public image: string,
    public isPublished: boolean,
  ){}
}

export interface ArtistData {
  [key: string]: any;
  name: string;
  information: string;
  image: File | null;
  album: {
    _id: string,
    title: string
  },
  isPublished: boolean,
}

export interface ApiArtistData {
  _id: string,
  name: string,
  information: string,
  image: string,
  isPublished: boolean,
}

export interface PublishArtist {
  id: string,
  isPublished: boolean
}
