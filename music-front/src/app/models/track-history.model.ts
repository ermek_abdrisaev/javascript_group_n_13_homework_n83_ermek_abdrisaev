export class TrackHistory {
  constructor(
    public _id: string,
    public user: string,
    public trackname: string,
  ){}
}

export interface TrackHistoryData {
  token: string;
  track: string;
}

export interface ApiTrackHistoryData {
  _id: string,
  user: string,
  trackname: string,
  datetime: string
}
