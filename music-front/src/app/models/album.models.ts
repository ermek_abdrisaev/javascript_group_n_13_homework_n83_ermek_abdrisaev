export class Album {
  constructor(
    public _id: string,
    public title: string,
    public artist: {
      _id: string,
      name:string
    },
    public release: number,
    public image: string,
    public isPublished: boolean,
  ){}
}

export interface AlbumData{
  [key: string]: any;
  _id: string,
  title: string;
  artist: {
    _id: string
    name:string
};
  release: number;
  image: File | null;
}

export interface ApiAlbumData {
  _id: string,
  title: string,
  artist: {
    _id: string,
    name:string
  },
  release: number,
  image: string,
  isPublished: boolean,
}

export interface PublishAlbum {
  id: string,
  isPublished: boolean
}
