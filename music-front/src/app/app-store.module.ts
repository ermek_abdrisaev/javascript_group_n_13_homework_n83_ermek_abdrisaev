import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { usersReducer } from './store/users.reducer';
import { localStorageSync } from 'ngrx-store-localstorage';
import { albumReducer } from './store/albums.reducer';
import { artistReducer } from './store/artists.reducer';
import { EffectsModule } from '@ngrx/effects';
import { AlbumsEffects } from './store/albums.effects';
import { ArtistsEffects } from './store/artists.effects';
import { UserEffects } from './store/user.effects';
import { NgModule } from '@angular/core';
import { TracksEffects } from './store/tracks.effects';
import { trackReducer } from './store/tracks.reducer';
import { TrackHistoryEffects } from './store/track-history.effects';
import { trackHistoryReducer } from './store/track-history.reducer';


const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true,
  })(reducer);
};

const metaReducers: MetaReducer[] = [localStorageSyncReducer];

const reducers = {
  albums: albumReducer,
  artists: artistReducer,
  users: usersReducer,
  tracks: trackReducer,
  trackHistory: trackHistoryReducer,
};

const effects = [AlbumsEffects, ArtistsEffects, UserEffects, TracksEffects, TrackHistoryEffects];

@NgModule({
  imports: [
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot(effects),
  ],
  exports: [StoreModule, EffectsModule],
})
export class AppStoreModule {}
