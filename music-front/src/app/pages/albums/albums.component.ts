import { Component, OnInit } from '@angular/core';
import { Album, PublishAlbum } from '../../models/album.models';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { deleteAlbumRequest, fetchAlbumsRequest, publishAlbumRequest } from '../../store/albums.actions';
import { ActivatedRoute } from '@angular/router';
import { Track } from '../../models/track.model';
import { deleteTrackRequest, fetchTracksRequest } from '../../store/tracks.actions';
import { User } from '../../models/user.model';
import { TrackHistory, TrackHistoryData } from '../../models/track-history.model';
import { createTrackHistoryRequest } from '../../store/track-history.actions';


@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.sass']
})
export class AlbumsComponent implements OnInit {
  trackHistory: Observable<TrackHistory[]>;
  user: Observable<User | null>;
  tracks: Observable<Track[]>;
  albums: Observable<Album[]>;
  publish: Observable<boolean>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  name!: string;
  token!: string;
  id!: string;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute) {
    this.user = store.select(state => state.users.user);
    this.albums = store.select(state => state.albums.albums);
    this.tracks = store.select(state => state.tracks.tracks);
    this.trackHistory = store.select(state => state.trackHistories.trackHistories);
    this.publish = store.select(state => state.albums.publishLoading);
    this.loading = store.select(state => state.albums.fetchLoading);
    this.error = store.select(state => state.albums.fetchError);
    this.loading = store.select(state => state.tracks.fetchLoading);
    this.error = store.select(state => state.tracks.fetchError);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      let id = params['id'];
      this.store.dispatch(fetchAlbumsRequest({id: id}));
    });

    this.albums.subscribe(album => {
      let id!: string;
      if (album) {
        id = album[0]?._id;
      }
      if (id !== undefined) {
        this.store.dispatch(fetchTracksRequest({id: id}));
      }
    });

    this.user.subscribe(user =>{
      if(user?.token){
        this.token = user.token;
      }
    });

  };

  onAdd(id: string){
    const trackHistory: TrackHistoryData = {
      track: id,
      token: this.token,
    };
    this.store.dispatch(createTrackHistoryRequest({trackHistory}));
  }

  onDelete(id: string){
    this.store.dispatch(deleteTrackRequest({id}));
  }

  onPublish(id: string){
    const changeStat: PublishAlbum = {
      id: id,
      isPublished: false,
    }
    this.store.dispatch(publishAlbumRequest({publish: changeStat }));
  }

  onDeleteAlbum(id: string){
    this.store.dispatch(deleteAlbumRequest({id}));
  }
}
