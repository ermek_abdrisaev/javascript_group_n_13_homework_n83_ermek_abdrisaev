import { Component, OnInit } from '@angular/core';
import { Track } from '../../models/track.model';
import { Observable } from 'rxjs';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { fetchTracksRequest, publishTracksRequest } from '../../store/tracks.actions';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../models/user.model';
import { PublishAlbum } from '../../models/album.models';
import { publishAlbumRequest } from '../../store/albums.actions';

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.sass']
})
export class TracksComponent implements OnInit {
  user: Observable<User | null>;
  tracks: Observable<Track[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  newLogged!: User | null;
  id!: string;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.user = store.select(state => state.users.user);
    this.tracks = store.select(state => state.tracks.tracks);
    this.loading = store.select(state => state.tracks.fetchLoading);
    this.error = store.select(state => state.tracks.fetchError);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params) {
        let id = params['id'];
        this.store.dispatch(fetchTracksRequest({id: id}));
      }
    });
  }

  onPublishTrack(id: string){
    const changeStat: PublishAlbum = {
      id: id,
      isPublished : true,
    }
    this.store.dispatch(publishTracksRequest({publish: changeStat }));
  }
}
