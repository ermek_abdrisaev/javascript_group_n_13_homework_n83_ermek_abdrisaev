import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Artist } from '../../../models/artist.model';
import { Album } from '../../../models/album.models';
import { AppState } from '../../../store/types';
import { Store } from '@ngrx/store';
import { fetchArtistsRequest } from '../../../store/artists.actions';
import { fetchAlbumsRequest } from '../../../store/albums.actions';
import { TrackData } from '../../../models/track.model';
import { ActivatedRoute } from '@angular/router';
import { createTracksRequest } from '../../../store/tracks.actions';

@Component({
  selector: 'app-new-track',
  templateUrl: './new-track.component.html',
  styleUrls: ['./new-track.component.sass']
})
export class NewTrackComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  artists!: Observable<Artist[]>;
  albums!: Observable<Album[]>;
  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.artists = store.select(state => state.artists.artists);
    this.albums = store.select(state => state.albums.albums);
    this.loading = store.select(state => state.tracks.createLoading);
    this.error = store.select(state => state.tracks.createError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchArtistsRequest());
  }

  getAlbumsOptions(id: string) {
    this.store.dispatch(fetchAlbumsRequest({id}));
  }
  onSubmit(){
    const trackData: TrackData = {
      trackname: this.form.value.trackname,
      album: this.form.value.album,
      duration: this.form.value.duration
    };
    this.store.dispatch((createTracksRequest({trackData})));
  }

}
