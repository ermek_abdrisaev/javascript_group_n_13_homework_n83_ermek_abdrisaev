import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/types';
import { AlbumData } from '../../../models/album.models';
import { createAlbumsRequest } from '../../../store/albums.actions';
import { Artist } from '../../../models/artist.model';
import { fetchArtistsRequest } from '../../../store/artists.actions';

@Component({
  selector: 'app-new-album',
  templateUrl: './new-album.component.html',
  styleUrls: ['./new-album.component.sass']
})
export class NewAlbumComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<string | null>;
  artists: Observable<Artist[]>;

  constructor(private store: Store<AppState>) {
    this.loading = store.select(state => state.albums.createLoading);
    this.error = store. select(state => state.albums.createError);
    this.artists = store.select(state => state.artists.artists);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchArtistsRequest());
  }

  onSubmit(){
    const albumData: AlbumData = this.form.value;
    this.store.dispatch(createAlbumsRequest({albumData}));
  }

}
