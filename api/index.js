const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const artists = require('./app/artists');
const albums = require('./app/albums');
const users = require('./app/users');
const tracks = require('./app/tracks');
const trackHistory = require('./app/track_history');
const app = express();

const port = 8080;

const whiteList = ['http://localhost:4200', 'https://localhost:4200'];
const corsOptions = {
  origin: (origin, callback) => {
    if(origin === undefined || whiteList.indexOf(origin) !== -1){
      callback(null, true);
    } else(new Error('Not allowed by CORS'));
  }
}

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.static('public'));
app.use('/albums', albums);
app.use('/artists', artists);
app.use('/users', users);
app.use('/tracks', tracks);
app.use('/track-history', trackHistory);

const run = async () =>{
  await mongoose.connect(config.mongo.db, config.mongo.options);

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  process.on('exit', ()=>{
    mongoose.disconnect();
  });
};

run().catch(e => console.error(e));
