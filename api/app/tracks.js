const express = require('express');
const Track = require("../models/Track");
const auth = require("../middlwear/auth");
const permit = require("../middlwear/permit");

const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    const query = {};
    if (req.query.album) {
      query.album = {_id: req.query.album}
    }
    const track = await Track.find(query).populate('album', 'title');
    return res.send(track);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, permit('admin', 'user'), async (req, res, next) => {
  try {
    if (!req.body.trackname) {
      return res.status(404).send({message: 'Fill required fields'});
    }
    const trackData = {
      trackname: req.body.trackname,
      album: req.body.album,
      duration: req.body.duration,
      isPublished: false,
    };
    const track = new Track(trackData);
    await track.save();
    return res.send({message: 'Created new track', id: track._id});
  } catch (e) {
    next(e);
  }
});

router.post('/:id/publish', auth, permit('admin'), async (req, res, next) => {
  try {
    await Track.updateOne({_id: req.params.id}, {isPublished: req.body.isPublished});
    return res.send({message: 'Published'});
  } catch (e) {
    next(e);
  }
});


router.delete('/:id', auth, permit('admin'), async(req, res, next) =>{
  try{
    if('admin' && 'user'){
      const track = await Track.deleteOne({_id: req.params.id});
      return res.send(track);
    }
  }catch(e){
    next(e);
  }
})

module.exports = router;