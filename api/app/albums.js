const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Album = require('../models/Album');
const permit = require("../middlwear/permit");
const auth = require("../middlwear/auth");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const query = {};

    if (req.query.artist) {
      const albumsId = await Album.find({artist: {_id: req.query.artist}}).populate('artist', 'name');
      return res.send(albumsId);
    }
    const albums = await Album.find(query).populate('artist', 'name');
    return res.send(albums);

  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const album = await Album.findById(req.params.id);

    if (!album) {
      return res.status(404).send({message: "Not found"});
    }
    return res.send(album);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, permit('admin', 'user'), upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.title || !req.body.artist) {
      return res.status(404).send({message: "Not found!"});
    }

    const albumData = {
      artist: req.body.artist,
      title: req.body.title,
      release: req.body.release,
      image: null,
      isPublished: false
    };

    if (req.file) {
      albumData.image = req.file.filename;
    }

    if (req.user.role === 'admin') {
      albumData.isPublished = true;
    }

    const album = new Album(albumData);
    await album.save();
    return res.send({message: "Created new album"});
  } catch (e) {
    next(e);
  }
});

router.post('/:id/publish', auth, permit('admin'), async (req, res, next) => {
  try {
    await Album.updateOne({_id: req.params.id}, {isPublished: req.body.isPublished});
    return res.send({message: 'Published'});
  } catch (e) {
    next(e);
  }
});

router.delete(':id', auth, permit('admin'), async(req, res, next) =>{
  try{
    if(req.body.user === 'admin'){
      const album = await Album.deleteOne({_id: req.params.id});
      return res.send(album);
    }
  }catch(e){
    next(e);
  }
})

module.exports = router;