const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const Artist = require('../models/Artist');
const auth = require("../middlwear/auth");
const permit = require("../middlwear/permit");
const Album = require("../models/Album");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) =>{
  try{
    const query = {};

    if(req.params.filter ==='image'){
      query.image = {$ne: null};
    }

    const artists = await Artist.find(query);
    return res.send(artists);

  } catch(e){
    next(e);
  }
});

router.get('/:id', async (req,res,next) =>{
  try{
    const artist = await Artist.findById(req.params.id);
    if(!artist){
      return res.status(404).send({message: 'Not found!'});
    }
    return res.send(artist);
  }catch(e){
    next(e);
  }
});

router.post('/', auth, permit('admin', 'user'), upload.single('image'), async(req, res, next) =>{
  try{
    if(!req.body.name || !req.body.information){
      return res.status(404).send({message: "Not found!"});
    }

    const artistData = {
      name: req.body.name,
      information: req.body.information,
      image: null,
      isPublished: false,
    };

    if(req.file){
      artistData.image = req.file.filename;
    }

    const artist = new Artist(artistData);
    await artist.save();

    return res.send({message:"Created new artist"});
  } catch(e){
    next(e);
  }
});

router.post('/:id/publish', auth, permit('admin'), async (req, res, next) => {
  try {
    await Artist.updateOne({_id: req.params.id}, {isPublished: req.body.isPublished});
    return res.send({message: 'Published'});
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', auth, permit('admin'), async(req, res, next) =>{
  try{
    const artist = await Artist.deleteOne({_id: req.params.id});
    return res.send(artist);
  }catch(e){
    next(e);
  }
})

module.exports = router;