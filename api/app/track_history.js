const express = require('express');
const auth = require("../middlwear/auth");
const TrackHistory = require('../models/TrackHistory');

const router = express.Router();


router.post('/', auth, async(req, res, next) =>{
  try{
    console.log(req.body);
    const trackHistoryData = {
      user: req.user._id,
      track: req.body.track,
      datetime: new Date().toISOString(),
    };


    const history = await new TrackHistory(trackHistoryData);
    await history.save();

    return res.send(history);
  }catch(e){
    next(e);
  }
});

module.exports = router;