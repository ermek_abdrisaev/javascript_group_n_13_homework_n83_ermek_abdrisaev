const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TrackSchema = new Schema({
  trackname: String,
  album:{
    type: Schema.Types.ObjectId,
    ref: 'Album'
  },
  duration: String,
  isPublished: {
    type: Boolean,
    required: true,
    default: false
  },
});

const Track = mongoose.model('Track', TrackSchema);
module.exports = Track;