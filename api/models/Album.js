const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
  artist:{
    type: Schema.Types.ObjectId,
    ref: 'Artist'
  },
  title:{
    type: String,
    require: true
  },
  track:{
    type: Schema.Types.ObjectId,
    ref: 'Track'
  },
  release: Number,
  image: String,
  isPublished: {
    type: Boolean,
    required: true,
    default: false
  },
});

const Album = mongoose.model('Album', AlbumSchema);
module.exports = Album;