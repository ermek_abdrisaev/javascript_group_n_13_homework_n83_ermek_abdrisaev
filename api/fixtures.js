const mongoose = require('mongoose');
const config = require('./config');
const Artist = require('./models/Artist');
const Album = require('./models/Album');
const User = require("./models/User");
const { nanoid } = require("nanoid");
const Track = require("./models/Track");

const run = async () =>{
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for(const coll of collections){
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [pac, snoop, dre] = await Artist.create({
    name: '2Pac',
    information: 'Ту́пак Ама́ру Шаку́р — хип-хоп-исполнитель, продюсер и актёр из Гарлема, Нью-Йорк, а позже — из Окленда, Калифорния. ',
    isPublished: true,
    image: '2Pac.jpeg'
  }, {
    name: 'Snoop Dog',
    information: 'Ке́лвин Кордоза́р Бро́дус-мла́дший, более известный как Снуп Догг, — американский рэпер, продюсер и актёр.',
    isPublished: true,
    image: 'Snoop Dogg.jpeg'
  }, {
    name: 'Dr.Dre',
    information: 'Андре Ромелл Янг, более известный под сценическим именем Доктор Дре — американский рэпер и продюсер, один из наиболее успешных битмейкеров в рэп-музыке',
    isPublished: false,
    image: 'Dr Dre.jpeg'
  });

  const [album, DS, DR] = await Album.create({
    artist: pac,
    title: 'All eyez on me',
    singer: '2Pac',
    release: 1993,
    isPublished: true,
    image: 'all eyez on me.jpeg'
  }, {
    artist: snoop,
    title: 'Doggy style',
    singer: 'Snoop Dogg',
    release: 1993,
    isPublished: true,
    image: 'Doggy style.jpeg'
  }, {
    artist: dre,
    title: 'Still Dre',
    singer:'Dr.Dre',
    release: 2001,
    isPublished: false,
    image: 'still Dre.jpg'
  });

  const [track1, track2, track3, track4, track5 ,track6 ,track7, track8, track9, track10] = await Track.create({
    trackname: 'Ambitionz az a Ridah',
    album: album,
    duration: '4:39',
    isPublished: true,
  }, {
    trackname: 'All bout you',
    album: DS,
    duration: '4:37',
    isPublished: true,
  },{
    trackname: 'California love',
    album: album,
    duration: '6:25',
    isPublished: false,
  }, {
    trackname: 'All Eyez on Me',
    album: album,
    duration: '5:08',
    isPublished: false,
  }, {
    trackname: 'Gin and Juice',
    album: DS,
    duration: '3:31',
    isPublished: true,

  }, {
    trackname: 'Who Am I (What’s My Name)?',
    album: DS,
    duration: '4:06',
    isPublished: true,
  }, {
    trackname: 'Doggy Dogg World',
    album: DS,
    duration: '5:05',
    isPublished: false,
  }, {
    trackname: 'Still D.R.E',
    album: DR,
    duration: '4:31',
    isPublished: true,
  },{
    trackname: 'The next episode',
    album: DR,
    duration: '2:42',
    isPublished: false,
  }, {
    trackname: 'Nuthin but a G Thang Let Me Ride',
    album: DR,
    duration: '3:59',
    isPublished: true,
  });

  await User.create({
    email: 'user@user.com',
    password: '123',
    token: nanoid(),
    role: 'user',
    displayName: 'Yusosha'
  }, {
    email: 'admin@admin.com',
    password: '123',
    token: nanoid(),
    role: 'admin',
    displayName: 'Adosha'
  })

  await mongoose.connection.close();
};

run().catch(e => console.error(e));